const dataLayer = require("companydata");
const Department = dataLayer.Department;
const Employee = dataLayer.Employee;
const Timecard = dataLayer.Timecard;
const COMPANY_NAME = "txg9567";//change to your liking
var moment = require('moment');
const CURRENT_DATE = moment().format("YYYY-MM-DD");
const CURRENT_DATETIME = moment().format("YYYY-MM-DD HH:mm:SS");
const dateFormat = require('date-fns/format');


//SECTION SAFE DELETE OPERATIONS
function deleteAllTimecards(timecards) {
    timecards.forEach(function (timecard) {
        dataLayer.deleteTimecard(timecard.getId());
    });
}

function deleteAllEmployeesFromDepartment(employees, dept_id) {
    employees.forEach(function (employee) {
        if (employee.getDeptId() == dept_id) {
            var timecards = dataLayer.getAllTimecard(employee.getId());
            deleteAllTimecards(timecards);
            dataLayer.deleteEmployee(employee.getId());
        }
    });
}

function deleteAllEmployees(employees) {
    employees.forEach(function (employee) {
        var timecards = dataLayer.getAllTimecard(employee.getId());
        deleteAllTimecards(timecards);
        dataLayer.deleteEmployee(employee.getId());
    });
}

//SECTION VALIDATE

/**
 * Checks if Timecard exists
 * @param {int} id 
 */
function doesTimecardExist(id) {
    var result = false;
    if (dataLayer.getTimecard(id) != null) {
        result = true;
    }
    return result;
}

/**
 * Checks if Employee exists
 * @param {int} id 
 */
function doesEmployeeExist(id) {
    var result = false;
    if (dataLayer.getEmployee(id) != null) {
        result = true;
    }
    return result;
}

/**
 * Validates a date format
 * @param {String} date 
 */
function isDateFormatValid(date) {
    var result = false;
    var regex = /^\d{4}-\d{2}-\d{2}$/;
    if (date.match(regex))
        result = true;
    return result;

}

/**
 * Validates a time format
 * @param {String} time 
 */
function isTimeFormatValid(time) {
    var result = false;
    var regex = new RegExp("([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])");
    if (regex.test(time)) {
        result = true;
    }
    return result;
}


/**
 * Checks if the department has the same employee
 * @param {int} dept_id 
 * @param {int} emp_id 
 */
function isManagerInDepartment(dept_id, emp_id) {
    var result = false;
    if (dataLayer.getEmployee(emp_id).getDeptId() == dept_id) {
        result = true;
    }
    return result;
}

/**
 * This one skips over the Timecard being updated, the rest is the same function as isStartTimeUnique()
 * @param {*} start_time 
 * @param {*} emp_id 
 * @param {*} timecard_id 
 */

function isStartTimeUniqueForUpdating(start_time, emp_id, timecard_id) {
    var result = true;
    var timecards = dataLayer.getAllTimecard(emp_id);
    var times = start_time.split(" ");
    timecards.forEach(function (timecard) {
        var checkTimes = timecard.getStartTime().split(" ");
        if (times[0] == checkTimes[0] && timecard_id !== timecard.getId()) {
            result = false;
        }
    });
    return result;
}

function isDeptNoUnique(company, dept_no) {
    var result = true;
    var departments = dataLayer.getAllDepartment(company);
    departments.forEach(function (department) {
        if (department.getDeptNo() == dept_no) {
            result = false;
        }
    });
    return result;
}

function isDeptNoUniqueForUpdating(company, dept_no, dept_id) {
    var result = true;
    var departments = dataLayer.getAllDepartment(company);
    departments.forEach(function (department) {
        if (department.getDeptNo() == dept_no && department.getId() !== dept_id) {
            result = false;
        }
    })
    return result;
}

function isEmpNoUnique(company, emp_no) {
    var result = true;
    var employees = dataLayer.getAllEmployee(company);
    employees.forEach(function (employee) {
        if (employee.getEmpNo() == emp_no) {
            result = false;
        }
    });
    return result;
}

function isEmpNoUniqueForUpdating(company, emp_no, emp_id) {
    var employees = dataLayer.getAllEmployee(company);
    employees.forEach(function (employee) {
        if (employee.getEmpNo() == emp_no && employee.getId() !== emp_id) {
            return false;
        }
    });
    return true;
}

function isStartTimeUnique(start_time, emp_id) {
    var result = true;
    var timecards = dataLayer.getAllTimecard(emp_id);
    var times = start_time.split(" ");
    timecards.forEach(function (timecard) {
        var checkTimes = timecard.getStartTime().split(" ");
        if (times[0] == checkTimes[0]) {
            result = false;
        }
    });
    return result;

}

function isStartTimeValid(start_time) {
    times = start_time.split(" ");
    var result = false;
    if (isDateFormatValid(times[0]) && isTimeFormatValid(times[1])) {
        if (moment(times[0], "YYYY-MM-DD").isSameOrBefore(moment().format("YYYY-MM-DD"))) {
            if (moment(times[0], "YYYY-MM-DD").isSameOrAfter(moment().subtract(7, "d"))) {
                if (moment(times[0]).day() !== 6 && moment(times[0]).day() !== 0) {
                    if (moment(times[1], "hh:mm:ss").isSameOrAfter(moment("06:00:00", "hh:mm:ss"))) {
                        if (moment(times[1], "hh:mm:ss").isSameOrBefore(moment("18:00:00", "hh:mm:ss"))) {
                            result = true;
                        }
                    }
                }
            }
        }
    }
    return result;
}

/**
 * Checks if end time is valid
 * @param {String} start_time 
 * @param {String} end_time 
 */
function isEndTimeValid(start_time, end_time) {
    result = false;
    var startTimes = start_time.split(" ");
    var endTimes = end_time.split(" ");
    if (isStartTimeValid(end_time) && moment(endTimes[0], "YYYY-MM-DD").isSame(startTimes[0], "YYYY-MM-DD") && moment(endTimes[1], "hh:mm:ss").isSameOrAfter(moment(startTimes[1], "hh:mm:ss").add(1, "h"))) {
        result = true;
    }
    return result;
}

/**
 * Checks if the date is on a weekend and/or after the current day
 * @param {moment} date 
 */
function isHireDateValid(date) {
    result = false;
    if (!date.isAfter(CURRENT_DATE) && date.day() !== 6 && date.day() !== 0) {
        result = true;
    }
    return result;

}

/**
 * Checks if the hire date is unique for that department
 * @param {String} hire_date 
 * @param {String} company 
 * @param {int} dept_id 
 */
function isHireDateUnique(hire_date, company, dept_id) {
    result = true;
    var employees = dataLayer.getAllEmployee(company)
    employees.forEach(function (employee) {
        if (hire_date == employee.getHireDate() && dept_id == employee.getDeptId()) {
            console.log(employee.getHireDate());
            result = false;
        }
    });
    return result;
}

/**
 * Checks if the hire date is unique, but skips over the current employee being updated
 * @param {String} hire_date 
 * @param {String} company 
 * @param {int} dept_id 
 * @param {int} emp_id 
 */
function isHireDateUniqueForUpdating(hire_date, company, dept_id, emp_id) {
    result = true;
    var employees = dataLayer.getAllEmployee(company)
    employees.forEach(function (employee) {
        if (hire_date == employee.getHireDate() && dept_id == employee.getDeptId() && employee.getId() !== emp_id) {
            result = false;
        }
    });
    return result;
}

/**
 * Checks if the Department exists
 * @param {int} id 
 * @param {string} company 
 */
function doesDepartmentExist(id, company) {
    var result = false;
    if (dataLayer.getDepartment(company, id) !== null) {
        result = true;
    }
    return result;
}

/**
 * Checks if the department has employees
 * @param {String} company 
 * @param {int} dept_id 
 */
function doesDepartmentHaveEmployees(company, dept_id) {
    var result = false;
    var employees = dataLayer.getAllEmployee(company);
    if (employees !== null) {
        employees.forEach(function (employee) {
            if (employee.getDeptId() == dept_id)
                result = true;
            return;
        });

    }
    return result;
}


//SECTION JSON_GENERATOR
/**
 * Puts a department in JSon
 * @param {Department} department 
 */
function generateDepartmentJson(department) {
    return "{\"department\":{\"dept_id\":" + department.getId() + ", \"company\":\"" + department.getCompany() + "\", \"dept_name\":\"" + department.getDeptName() + "\", \"dept_no\":\"" + department.getDeptNo() + "\", \"location\":\"" + department.getLocation() + "\"}}";
}

/**
 * Puts an Employee in JSon
 * @param {Employee} employee 
 */
function generateEmployeeJson(employee) {
    return "{\"employee\":{\"emp_id\":" + employee.getId() + ",\"emp_name\":\"" + employee.getEmpName() + "\", \"emp_no\":\"" + employee.getEmpNo() + "\", \"hire_date\":\"" + employee.getHireDate() + "\", \"job\":\"" + employee.getJob() + "\", \"salary\":" + employee.getSalary() + ", \"dept_id\":" + employee.getDeptId() + ", \"mng_id\":" + employee.getMngId() + "}}";
}

/**
 * Checks if the value is null or an empty string
 * @param {boolean} val 
 */
function isEmpty(val) {
    result = false;
    if (val === null || val === "") {
        result = true;
    }
    return result;
}

/**
 * Puts a Timecard in JSon
 * @param {Timecard} Timecard 
 */
function generateTimecardJson(timecard) {
    return "{\"timecard\":{\"timecard_id\":" + timecard.getId() + ", \"start_time\":\"" + timecard.getStartTime() + "\", \"end_time\":\"" + timecard.getEndTime() + "\", \"emp_id\":" + timecard.getEmpId() + "}}";
}

/**
 * Generates a success message in Json Format
 * @param {String} message 
 */
function generateSuccessMessage(message) {
    return "{\"success\":" + message + "}";
}

/**
 * Generates an error message in Json Format
 * @param {String} message 
 */
function generateErrorMessage(message) {
    return "{\"error\":" + message + "}";
}

module.exports = {

    //SECTION INSERT
    /**
     * Inserts Department
     * @param {String} inJson 
     */
    insertDepartment: function (inJson) {
        var result = "";
        var jsonString = JSON.parse(inJson);

        if (jsonString.hasOwnProperty("company") && jsonString.hasOwnProperty("dept_name") && jsonString.hasOwnProperty("dept_no") && jsonString.hasOwnProperty("location")) {
            var company = jsonString["company"];
            var dept_name = jsonString["dept_name"];
            var dept_no = company + "-" + jsonString["dept_no"];
            var location = jsonString["location"];



            try {
                if (!isDeptNoUnique(company, dept_no)) {
                    return generateErrorMessage("\"Invalid Department number.\"");
                }
                var department = new Department(company, dept_name, dept_no, location);
                department = dataLayer.insertDepartment(department);

                if (department.getId() > 0 || department.getId() !== null) {
                    var resultValue = generateDepartmentJson(department);
                    result = generateSuccessMessage(resultValue);
                }
                else {
                    result = generateErrorMessage("\"Insert Failed!\"");
                }
            }
            catch (error) {
                result = generateErrorMessage("\"" + error + "\"");
            }
        }
        else {
            result = generateErrorMessage("\"Invalid JSON String\"");
        }
        return result;
    },

    /**
     * Inserts employee
     * @param {String} inJson 
     */
    insertEmployee: function (inJson) {
        var result = "";
        try {
            var jsonString = JSON.parse(inJson);
            if (jsonString.hasOwnProperty("emp_name") && jsonString.hasOwnProperty("emp_no") && jsonString.hasOwnProperty("hire_date") && jsonString.hasOwnProperty("job") && jsonString.hasOwnProperty("salary") && jsonString.hasOwnProperty("dept_id") && jsonString.hasOwnProperty("mng_id")) {

                var emp_name = jsonString["emp_name"];
                var emp_no = jsonString["emp_no"];
                var hire_date = jsonString["hire_date"];
                var date = moment(hire_date);
                var job = jsonString["job"];
                var salary = jsonString["salary"];
                var dept_id = jsonString["dept_id"];
                var mng_id = jsonString["mng_id"];



                //employee.setEmpName(emp_name);
                //employee.setJob(job);
                //employee.setSalary(salary);


                if (dataLayer.getDepartment(COMPANY_NAME, dept_id) !== null) {
                    if (emp_no.includes(COMPANY_NAME + "-") && isEmpNoUnique(COMPANY_NAME, emp_no)) {

                        //employee.setEmpNo(emp_no);
                        //if there are no employees in the department, mng id will be set to 0
                        if (doesDepartmentHaveEmployees(COMPANY_NAME, dept_id)) {
                            if (doesEmployeeExist(mng_id)) {
                                //employee.setMngId(mng_id);
                            }
                            else {
                                result = generateErrorMessage("\"Employee " + mng_id + " not found.\"");
                                return result;
                            }
                        }
                        else {
                            mng_id = 0
                        }

                        if (doesDepartmentExist(dept_id, COMPANY_NAME)) {
                            //employee.setDeptId(dept_id);
                            if (isHireDateValid(date) && isHireDateUnique(date, COMPANY_NAME, dept_id)) {
                                //console.log(isHireDateUnique(hire_date,COMPANY_NAME,dept_id));
                                //employee.setHireDate(date.format("YYYY-MM-DD"));
                                //new Employee(emp_name="", emp_no="", hire_date=format(new Date(), 'YYYY-MM-DD'),
                                //              job="", salary=0.00, dept_id= null, mng_id=null, emp_id=null)

                                console.log(moment(hire_date).format("YYYY-MM-DD"));
                                var employee = new Employee(emp_name,emp_no,dateFormat(hire_date, "YYYY-MM-DD" ),job,salary,dept_id,mng_id,null);
                                employee = dataLayer.insertEmployee(employee);

                                if (employee.getId() > 0 && employee.getId() != null) {
                                    var newEmployee = dataLayer.getEmployee(employee.getId());
                                    console.log("new hire date: " + newEmployee.getHireDate());
                                    result = generateSuccessMessage(generateEmployeeJson(newEmployee));
                                }
                                else {
                                    result = generateErrorMessage("\"Insert Failed!\"");
                                }
                            }
                            else {
                                result = generateErrorMessage("\"Date has to be before " + CURRENT_DATE + " on a weekday and has to be unique for the department!\"");
                            }

                        }
                        else {
                            result = generateErrorMessage("\"Department" + dept_id + " of" + COMPANY_NAME + " not found.\"");
                        }

                    }
                    else {
                        result = generateErrorMessage("\"Invalid Employee Number.\"");
                    }
                }
                else {
                    result = generateErrorMessage("\"Department" + dept_id + " not found.\"");
                }
            }
            else {
                result = generateErrorMessage("\"Invalid Json values!\"");
            }
        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
            console.log(error);
        }

        return result;
    },

    /**
     * Inserts a Timecard
     * @param {String} inJson 
     */
    insertTimecard: function (inJson) {
        var result = "";
        var jsonString = JSON.parse(inJson);

        try {
            if (jsonString.hasOwnProperty("emp_id") && jsonString.hasOwnProperty("start_time") && jsonString.hasOwnProperty("end_time")) {
                var emp_id = jsonString["emp_id"];
                var start_time = jsonString["start_time"];
                var end_time = jsonString["end_time"];

                //console.log("Start:" + start_time + "\nEnd:" + end_time);
                if (doesEmployeeExist(emp_id)) {

                    if (isStartTimeValid(start_time) && isStartTimeUnique(start_time, emp_id)) {
                        if (isEndTimeValid(start_time, end_time)) {
                            var time = new Timecard(start_time, end_time, emp_id);

                            time = dataLayer.insertTimecard(time);
                            var timeAfter = dataLayer.getTimecard(time.getId());


                            result = generateSuccessMessage(generateTimecardJson(timeAfter));
                        }
                        else {
                            result = generateErrorMessage("\"End time " + end_time + " is invalid. Has to be at most a week from the current day, on a weekday, unique and between 6:00 and 18:00 and at least an hour after start time.\"");
                        }
                    }
                    else {
                        result = generateErrorMessage("\"Start time " + start_time + " is invalid. Has to be on the same day as start time, at least an hour later, on a weekday, unique and between 6:00 and 18:00.\"");
                    }
                }
                else {
                    result = generateErrorMessage("\"Employee " + emp_id + " not found.\"");
                }
            }
            else {
                result = generateErrorMessage("\"Invalid JSon.\"");
            }
        }

        catch (error) {
            //result = generateErrorMessage("\"" + error + "\"");
            result = error;
        }
        return result;

    },

    //SECTION UPDATE
    /**
     * Updates Department
     * @param {int} dept_id 
     * @param {String} company 
     * @param {String} dept_name 
     * @param {String} dept_no 
     * @param {String} location 
     */
    updateDepartment: function (dept_id, company, dept_name, dept_no, location) {
        var result = "";
        try {
            var department = dataLayer.getDepartment(company, dept_id);
            if (department != null) {
                if (company !== null || company !== "") {
                    department.setCompany(company);
                }
                if (dept_name !== null || dept_name !== "") {
                    department.setDeptName(dept_name);
                }
                if (dept_no !== null || dept_name !== "") {
                    if (!isDeptNoUniqueForUpdating(COMPANY_NAME, dept_no, dept_id)) {
                        return generateErrorMessage("\"Invalid Department number.\"");
                    }
                    department.setDeptNo(department.getCompany() + "-" + dept_no);
                }
                if (location !== null || location !== "") {
                    department.setLocation(location);
                }

                department = dataLayer.updateDepartment(department);

                result = generateSuccessMessage(generateDepartmentJson(department));
            }
            else {
                result = generateErrorMessage("\"Department " + dept_id + " of " + company + " not found!\"");
            }
        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
        }

        return result;
    },

    //res.end(business.updateEmployee(req.body["emp_id"], req.body["emp_name"], req.body["emp_no"], req.body["hire_date"], req.body["job"], req.body["salary"], req_body["dept_id"], req.body["mng_id"])); 
    /**
     * 
     * @param {*} emp_id 
     * @param {*} emp_name 
     * @param {*} emp_no 
     * @param {*} hire_date 
     * @param {*} job 
     * @param {*} salary 
     * @param {*} dept_id 
     * @param {*} mng_id 
     */
    updateEmployee: function (emp_id, emp_name, emp_no, hire_date, job, salary, dept_id, mng_id) {
        var result;
        try {
            if (doesEmployeeExist(emp_id)) {
                employee = dataLayer.getEmployee(emp_id);
                if (!isEmpty(emp_name)) {
                    employee.setEmpName(emp_name);
                }
                if (!isEmpty(emp_no)) {
                    if (emp_no.includes(COMPANY_NAME + "-")) {
                        employee.setEmpNo(emp_no);

                    }
                    else {
                        result = generateErrorMessage("\"Invalid employee number\"");
                        return result;
                    }
                }
                if (!isEmpty(hire_date)) {
                    var date = moment(hire_date);
                    if (isHireDateValid(date) && isHireDateUniqueForUpdating(COMPANY_NAME, employee.getDeptId(), employee.getId())) {
                        employee.setHireDate(date.format("YYYY-MM-DD"));
                    }
                    else {
                        result = generateErrorMessage("\"Date has to be before " + CURRENT_DATE + " on a weekday and has to be unique for the department!\"");
                        return result;
                    }
                }

                if (!isEmpty(job)) {
                    employee.setJob(job);
                }

                if (!isEmpty(salary)) {
                    employee.setSalary(salary);
                }

                if (!isEmpty(dept_id)) {
                    if (doesDepartmentExist(dept_id, COMPANY_NAME)) {
                        employee.setDeptId(dept_id);
                    }

                    else {
                        result = generateErrorMessage("\"Employee " + emp_id + " not found!\"");
                        return result;
                    }

                }
                if (!isEmpty(mng_id)) {
                    if (doesEmployeeExist(mng_id) && isManagerInDepartment(employee.getDeptId(), mng_id)) {
                        employee.setMngId(mng_id);
                    }
                    else {
                        result = generateErrorMessage("\"Manager " + mng_id + " not found in department" + employee.getDeptId() + "!\"");
                        return result;
                    }
                }

                employee = dataLayer.updateEmployee(employee);
                result = generateSuccessMessage(generateEmployeeJson(employee));
            }
            else {
                result = generateErrorMessage("\"Employee " + emp_id + " not found!\"");
            }

        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
            //result = error;
        }
        return result;
    },

    updateTimecard: function (timecard_id, emp_id, start_time, end_time) {
        var result = "";
        try {
            if (doesTimecardExist(timecard_id)) {
                timecard = dataLayer.getTimecard(timecard_id);
                if (emp_id !== null) {
                    if (doesEmployeeExist(emp_id)) {
                        timecard.setEmpId(emp_id);
                    }
                    else {
                        result = generateErrorMessage("\"Employee " + emp_id + " not found!\"");
                        return result;
                    }
                }
                if (!isEmpty(start_time)) {
                    if (isStartTimeUniqueForUpdating(start_time, timecard.getEmpId(), timecard.getId()) && isStartTimeValid(start_time)) {
                        timecard.setStartTime(start_time);
                    }
                    else {
                        result = generateErrorMessage("\"Start time " + start_time + " is invalid. Has to be on the same day as start time, at least an hour later, on a weekday, unique and between 6:00 and 18:00.\"");
                    }
                }
                if (!isEmpty(end_time)) {
                    if (isEndTimeValid(timecard.getStartTime(), end_time)) {
                        timecard.setEndTime(end_time);
                    }
                    else {
                        result = generateErrorMessage("\"End time " + end_time + " is invalid. Has to be at most a week from the current day, on a weekday, unique and between 6:00 and 18:00 and at least an hour after start time.\"");
                    }
                }

                timecard = dataLayer.updateTimecard(timecard);
                timecardResult = dataLayer.getTimecard(timecard.getId());
                result = generateSuccessMessage(generateTimecardJson(timecardResult));

            }
            else {
                result = generateErrorMessage("\"Timecard " + timecard_id + " not found!\"");
            }
        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
            //result = error;
        }
        return result;

    },

    //SECTION GETALL
    /**
     * Gets All Departments
     * @param {String} company 
     */
    getAllDepartments: function (company) {
        var departments = dataLayer.getAllDepartment(company);
        var result;
        if (departments.length > 0) {
            result = "[";
            departments.forEach(function (department) {
                result += generateDepartmentJson(department) + ",";
            });
            result = result.substring(0, result.length - 1);
            result += "]";

        }
        else {
            result = generateErrorMessage("\"Departments of " + company + " not found!\"");
        }
        return result;
    },

    /**
     * Gets All Employees
     * @param {String} company 
     */
    getAllEmployees: function (company) {
        var employees = dataLayer.getAllEmployee(company);
        var result;
        if (employees.length > 0) {
            result = "[";
            employees.forEach(function (employee) {
                result += generateEmployeeJson(employee) + ",";
            });
            result = result.substring(0, result.length - 1);
            result += "]";

        }
        else {
            result = generateErrorMessage("\"Employees of " + company + " not found!\"");
        }
        return result;
    },

    /**
     * Gets All Timecards
     * @param {int} emp_id 
     */
    getAllTimecards: function (emp_id) {
        var timecards = dataLayer.getAllTimecard(emp_id);
        var result;
        if (timecards.length > 0) {
            result = "[";
            timecards.forEach(function (timecard) {
                result += generateTimecardJson(timecard) + ",";
            });
            result = result.substring(0, result.length - 1);
            result += "]";

        }
        else {
            result = generateErrorMessage("\"Timecards of employee " + emp_id + " not found!\"");
        }
        return result;
    },




    //SECTION GET
    /**
     * Gets Department
     * @param {String} company 
     * @param {int} dept_id 
     */
    getDepartment: function (company, dept_id) {
        var result;

        try {
            var department = dataLayer.getDepartment(company, dept_id);
            if (department == null) {
                result = generateErrorMessage("\"Department" + dept_id + " of " + company + " not found!\"");
            }
            else {
                result = generateDepartmentJson(department);
            }

        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
        }
        return result;
    },

    /**
     * Gets Employee
     * @param {int} emp_id 
     */
    getEmployee: function (emp_id) {
        var result;

        try {
            var employee = dataLayer.getEmployee(emp_id);
            if (employee == null) {
                result = generateErrorMessage("\"Employee " + emp_id + " not found!\"");
            }
            else {
                result = generateEmployeeJson(employee);
            }

        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
        }
        return result;
    },

    /**
     * Gets Timecard
     * @param {int} timecard_id 
     */
    getTimecard: function (timecard_id) {
        var result;

        try {
            var timecard = dataLayer.getTimecard(timecard_id);
            result = generateTimecardJson(timecard);
        }
        catch (error) {
            result = generateErrorMessage("\"" + error + "\"");
        }
        return result;
    },

    //SECTION DELETE
    /**
     * Deletes all data by companyName
     * @param {String} companyName 
     */
    deleteAll: function (companyName) {
        var result = "";
        //var departments = dataLayer.getAllDepartment(companyName);
        var employees = dataLayer.getAllEmployee(companyName);
        deleteAllEmployees(employees);

        if (dataLayer.deleteCompany(companyName) > 0)
            result = generateSuccessMessage("\"" + companyName + "'s information deleted.\"");
        else
            result = generateErrorMessage("\"Failed to delete company " + companyName + ".\"");

        return result;
    },

    /**
     * Deletes an Employee
     * @param {int} emp_id 
     */
    deleteEmployee: function (emp_id) {
        var timecards = dataLayer.getAllTimecard(emp_id);
        deleteAllTimecards(timecards);
        if (dataLayer.deleteEmployee(emp_id) > 0) {
            return generateSuccessMessage("\"Employee " + emp_id + " deleted.\"");
        }
        else {
            return generateErrorMessage("\"Failed to delete Employee " + emp_id + ".\"");
        }
    },

    /**
     * Deletes a timecard
     * @param {int} timeId 
     */
    deleteTimecard: function (timeId) {
        if (dataLayer.deleteTimecard(timeId) > 0) {
            return generateSuccessMessage("\"Timecard " + timeId + " deleted.\"");
        }
        else {
            return generateErrorMessage("\"Failed to delete Timecard " + timeId + ".\"");
        }
    },

    /**
     * Deletes a Department
     * @param {String} inJson 
     */
    deleteDepartment: function (company, dept_id) {
        //var jsonString = JSON.parse(inJson);
        var result;
        //var company = jsonString["company"];
        //var dept_id = jsonString["dept_id"];
        var employees = dataLayer.getAllEmployee(company);
        deleteAllEmployeesFromDepartment(employees, dept_id);

        if (dataLayer.deleteDepartment(company, dept_id) > 0) {
            result = generateSuccessMessage("\"Department " + dept_id + " from " + company + " deleted.\"");
        }
        else {
            result = generateErrorMessage("\"Failed to delete " + dept_id + " from " + company + "\"");
        }

        return result;
    }


};

//test
/*
var currentDept = new Department("txg9567", "IT", "nijk", "rochester");
currentDept = dataLayer.insertDepartment(currentDept);
if (currentDept.getId() > 0) {
console.log("inserted Department id: " + currentDept.getId());
} else {
console.log("Not inserted");
}*/
//console.log(isDeptNoUniqueForUpdating("txg9567", "txg9567-g65", 1770));
console.log(isHireDateUnique("2018-12-09", COMPANY_NAME, 1762));
//console.log(insertDepartment("{\"company\":\"txg9567\", \"dept_name\":\"killme\", \"dept_no\":\"n66\",\"location\":\"hell\"}"));
//console.log(getDepartment("txg9567",1656));
//console.log(deleteDepartment("{\"company\":\"txg9567\", \"dept_id\":1645}"));
//console.log(deleteAll("txg9567"));

//console.log(getAllDepartments("txg9567"));
//console.log(updateDepartment(1644, "txg9567", "BroFORCE", "a31", "Manitoba"));

//console.log(insertEmployee("{\"emp_name\":\"Fugo\", \"emp_no\":\"txg9567-ga3\", \"hire_date\":\"2018-12-03\", \"job\":\"GangStar\", \"salary\":100.10, \"dept_id\":1646, \"mng_id\":1549}"));
//console.log(dataLayer.getEmployee(999999999));
//console.log(doesDepartmentHaveEmployees(COMPANY_NAME,1646));

//console.log(getAllEmployees("txg9567"));
//console.log(insertTimecard("{\"emp_id\":1537,\"start_time\":\"2018-12-04 10:00:00\",\"end_time\":\"2018-12-04 12:00:00\"}"));
//console.log(getDepartment("txg9567", 1646));
//console.log(isStartTimeValid("2018-12-05 18:00:00"));
//console.log(updateEmployee(1552,"Leon","txg9567-66","2018-12-04","Cop",333.33,1646,1549));
//console.log(deleteEmployee(1552));
//tim= new Timecard(moment().format("YYYY-MM-DD hh:mm:ss","2018-12-06 10:00:00"),moment().format("YYYY-MM-DD hh:mm:ss","2018-12-06 10:00:00"), 1646 );
//console.log(dataLayer.insertTimecard(tim));
/*
var tc = new Timecard("2018-12-04 10:30:00",
"2018-12-04 17:30:00",
1536);
tc = dataLayer.insertTimecard(tc);
if (tc.getId() > 0) {
console.log("inserted id: " + tc.getStartTime());
} else {
console.log("Not inserted");
}*/
//console.log(getTimecard(743));
//console.log(getAllTimecards(1536));
//console.log(getTimecard(739));
//console.log(deleteTimecard(739));
//console.log(updateTimecard(746, 1537, "2018-12-04 12:30:00", "2018-12-04 14:30:00"));