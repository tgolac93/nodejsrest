const business = require("./EmployeeBusiness");
const express = require('express');
const bodyParser = require("body-parser");
const MongoClient = require('mongodb').MongoClient;
//const bodyParser = require('body-parser');

const app = express();
const contextPath = "GolacTP4";
const rootPath = "CompanyServices";
const address = "/" + contextPath + "/" + rootPath + "/";

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.use(bodyParser());


//SECTION GET_ALL
/**
 * Get All Departments
 */
app.get(address + "departments", (req, res) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getAllDepartments(req.query.company));
});


/**
 * Get All Employees
 */
app.get(address + "employees", (req, res) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getAllEmployees(req.query.company));
});

/**
 * Get All Timecards
 */
app.get(address + "timecards", (req, res) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getAllTimecards(req.query.emp_id));
});

//SECTION GET
/**
 * Get Department
 */
app.get(address + "department", (req, res) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getDepartment(req.query.company, req.query.dept_id));
});

/**
 * Get Employee
 */
app.get(address + "employee", (req, res) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getEmployee(req.query.emp_id));
});

/**
 * Get Timecard
 */
app.get(address + "timecard", (req, res, next) => {
    //console.log(JSON.parse(business.getAllDepartments(req.query.company)));
    //console.log(business.getAllDepartments(req.query.company));
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.getTimecard(req.query.timecard_id));
});


//SECTION INSERT
/**
 * Insert department
 */
app.put(address + "department", (req, res) => {

    //console.log(req.body);
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.insertDepartment(JSON.stringify(req.body)));
});

/**
 * Insert employee
 */
app.put(address + "employee", (req, res) => {

    //console.log(req.body);
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.insertEmployee(JSON.stringify(req.body)));
});

/**
 * Insert Timecard
 */
app.put(address + "timecard", (req, res) => {

    //console.log(req.body);
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(business.insertTimecard(JSON.stringify(req.body)));
});

//SECTION UPDATE

/**
 * Update Department
 */
app.post(address + "department", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    //console.log(req.body["dept_id"]);
    res.end(business.updateDepartment(req.body["dept_id"],req.body["company"],req.body["dept_name"],req.body["dept_no"], req.body["location"]));
});

/**
 * Update Employee
 */
app.post(address + "employee", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    //console.log(req.body["emp_id"]);
    res.end(business.updateEmployee(Number(req.body["emp_id"]), String(req.body["emp_name"]), String(req.body["emp_no"]), String(req.body["hire_date"]), String(req.body["job"]), Number(req.body["salary"]), Number(req.body["dept_id"]), Number(req.body["mng_id"]))); 
});

/**
 * Update Timecard
 */
app.post(address + "timecard", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    //console.log(req.body["emp_id"]);
    res.end(business.updateTimecard(req.body["timecard_id"],req.body["emp_id"],req.body["start_time"], req.body["end_time"])); 
});

//SECTION DELETE
/**
 * Delete Department
 */
app.delete(address + "department", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    
    res.end(business.deleteDepartment(req.query.company, req.query.dept_id)); 
});

/**
 * Delete Employee
 */
app.delete(address + "employee", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    
    res.end(business.deleteEmployee(req.query.emp_id)); 
});

/**
 * Delete Timecard
 */
app.delete(address + "timecard", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    
    res.end(business.deleteTimecard(req.query.timecard_id)); 
});

/**
 * Delete Company
 */
app.delete(address + "company", (req, res)=>{
    res.writeHead(200, { "Content-Type": "application/json" });
    
    res.end(business.deleteAll(req.query.company)); 
});