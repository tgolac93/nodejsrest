// const format = require('date-fns/format');
// const parse = require('date-fns/parse');

// function isValidDate(dateString) {
//     var regEx = /^\d{4}-\d{2}-\d{2}$/;
//     if(!dateString.match(regEx)) return false;  // Invalid format
//     var d = new Date(dateString);
//     if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
//     return d.toISOString().slice(0,10) === dateString;
// }

// function validateTime(strTime) {

//     var regex = new RegExp("([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])");

//     if (regex.test(strTime)) {
//         return true;
//     } else {
//         return false;
//     }

// }

// var testDate = '2015-02-28 24:30:00';

// var parts = testDate.split(" ");

// console.log(validateTime(parts[1]));
// console.log(isValidDate(parts[0]));

// console.log("done");

// var str = format(new Date(), 'YYYY-MM-DD');

// console.log(typeof str);
// console.log(str);

// var result = parse('2013-02-30 13:30:30')
// console.log(result);
// console.log(typeof result);

const dataLayer = require("companydata");
const Department = dataLayer.Department;
const Employee = dataLayer.Employee;
const Timecard = dataLayer.Timecard;

console.log(dataLayer.deleteCompany("txg9567"));

//Department -------------------------------------------------------------------

var currentDept = new Department("txg9567", "IT", "txg9567-42", "rochester");
currentDept = dataLayer.insertDepartment(currentDept);
if (currentDept.getId() > 0) {
    console.log("inserted Department id: " + currentDept.getId());
} else {
    console.log("Not inserted");
}

console.log("---> getAllDepartment");
var departments = dataLayer.getAllDepartment("txg9567");
for (var d of departments) {
    console.log(d.getId());
    console.log(d.getCompany());
    console.log(d.getDeptName());
    console.log(d.getDeptNo());
    console.log(d.getLocation());
    console.log("--------\n\n");
}

var department = dataLayer.getDepartment("txg9567", currentDept.getId());

//Print the department details
console.log("\n\nCurrent Department:");
console.log(department.getId());
console.log(department.getCompany());
console.log(department.getDeptName());
console.log(department.getDeptNo());
console.log(department.getLocation());

department.setDeptName("Computing");
department = dataLayer.updateDepartment(department);

//Print the updated department details
console.log("\n\nUpdated Department:");
console.log(department.getId());
console.log(department.getCompany());
console.log(department.getDeptName());
console.log(department.getDeptNo());
console.log(department.getLocation());

var deleted = dataLayer.deleteDepartment("txg9567", currentDept.getId());
if (deleted >= 1) {
    console.log("\nDepartment deleted");
} else {
    console.log("\nDepartment not deleted");

}


/*
 //Employee----------------------------------------------------------------------
 //new Employee(emp_name="", emp_no="", hire_date=format(new Date(), 'YYYY-MM-DD'),
 //              job="", salary=0.00, dept_id= null, mng_id=null, emp_id=null)
 
 var emp = new Employee("French", "e55", "2018-10-22", "Developer", 80000.00, currentDept.getId());
 emp = dataLayer.insertEmployee(emp);
 if (emp.getId() > 0) {
 console.log("inserted Employee : " + emp.getId());
 } else {
 console.log("Not inserted");
 }
 
 console.log("\n---> getAllEmployee");
 var employees = dataLayer.getAllEmployee("txg9567");
 for (var emp of employees) {
 console.log(emp.getId());
 console.log(emp.getEmpName());
 console.log(emp.getEmpNo());
 console.log(emp.getHireDate());
 console.log(emp.getJob());
 console.log(emp.getSalary());
 console.log(emp.getDeptId());
 console.log(emp.getMngId());
 console.log("--------\n\n");
 }
 
 var employee = dataLayer.getEmployee(emp.getId());
 
 //Print the employee details
 console.log(employee.getId());
 console.log(employee.getEmpName());
 console.log(employee.getEmpNo());
 console.log(employee.getHireDate());
 console.log(employee.getJob());
 console.log(employee.getSalary());
 console.log(employee.getDeptId());
 console.log(employee.getMngId());
 console.log("--------\n\n");
 
 
 employee.setSalary(60000.00);
 employee = dataLayer.updateEmployee(employee);
 
 //Print the updated employee details
 console.log("\n\nUpdated Employee:");
 console.log(employee.getId());
 console.log(employee.getEmpName());
 console.log(employee.getEmpNo());
 console.log(employee.getHireDate());
 console.log(employee.getJob());
 console.log(employee.getSalary());
 console.log(employee.getDeptId());
 console.log(employee.getMngId());
 console.log("--------\n\n");
 
 var deletedEmp = dataLayer.deleteEmployee(1395);
 if (deletedEmp >= 1) {
 console.log("\nEmployee deleted");
 } else {
 console.log("\nEmployee not deleted");
 }
 */

/*
 
 //Timecard----------------------------------------------------------------------
 */
 var tc = new Timecard("2018-12-04 10:30:00",
 "2018-12-04 17:30:00",
 1536);
 tc = dataLayer.insertTimecard(tc);
 if (tc.getId() > 0) {
 console.log("inserted id: " + tc.getId());
 } else {
 console.log("Not inserted");
 }
 /*
 
 var timecards = dataLayer.getAllTimecard(2);
 
 for (var tcard of timecards) {
 console.log(tcard.getId());
 console.log(tcard.getStartTime());
 console.log(tcard.getEndTime());
 console.log(tcard.getEmpId());
 console.log("--------\n\n");
 }
 
 var timecard = dataLayer.getTimecard(1);
 console.log("\n\nCurrent Timecard:");
 console.log(timecard.getId());
 console.log(timecard.getStartTime());
 console.log(timecard.getEndTime());
 console.log(timecard.getEmpId());
 console.log("--------\n\n");
 
 timecard.setEndTime("2018-06-19 19:30:00");
 
 timecard = dataLayer.updateTimecard(timecard);
 
 console.log("\n\nUpdated Timecard:");
 console.log(timecard.getId());
 console.log(timecard.getStartTime());
 console.log(timecard.getEndTime());
 console.log(timecard.getEmpId());
 console.log("--------\n\n");
 
 var deletedTC = dataLayer.deleteTimecard(1);
 if (deletedTC >= 1) {
 console.log("\nTimecard deleted");
 } else {
 console.log("\nTimecard not deleted");
 }
 */

